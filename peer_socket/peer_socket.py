# first of all import the socket library 
import socket
from threading import Thread

import jsonpickle
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

from peer_socket.socket_message import SocketMessage

GET_KEY = 'GET_KEY'

class PeerSocket:
    def __init__(self, addr, debug=True):
        self.callbacks = {}
        self.debug = debug
        self.addr = addr
        self.sender_addr = ()
        thread = Thread(target=self.runner, args=(self.addr,))
        thread.start()
        self.my_key_pair = RSA.generate(3072)

    def __debug(self, msg):
        if self.debug:
            print(str(self.addr) + ': ' + msg)

    def get_public_key(self):
        return self.my_key_pair.publickey().exportKey()

    def runner(self, addr):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind(addr)
        self.server.listen()
        while True:
            conn, addr = self.server.accept()
            buffer = conn.recv(1024)
            if len(buffer) > 0:
                json = buffer.decode('UTF-8')
                encrypted_message = jsonpickle.decode(json)

                # hardcode key exchange
                if encrypted_message.event == GET_KEY:
                    response = self.get_public_key()
                    json = jsonpickle.encode(response)
                    buffer = json.encode('UTF-8')
                    conn.send(buffer)
                else:

                    decryptor = PKCS1_OAEP.new(self.my_key_pair)
                    response = str(decryptor.decrypt(encrypted_message.payload))
                    self.__debug('Got request ' + encrypted_message.event + ' from peer ' + str(encrypted_message.sender_addr) + '.')
                    self.__debug(f'{response}\n--\n')

            conn.close()

    def on(self, event, f):
        self.callbacks[event] = f

    def __get_destination_key(self, dest_addr):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(dest_addr)
        key = SocketMessage(self.addr, GET_KEY, '')
        json = jsonpickle.encode(key)
        buffer = json.encode('UTF-8')
        client.send(buffer)
        buffer = client.recv(1024)
        if len(buffer) > 0:
            json = buffer.decode('UTF-8')
            message = jsonpickle.decode(json)
            self.__debug('Got response from peer ' + str(dest_addr) + '.')
            self.__debug(str(key))
            key = RSA.import_key(message)
            return key
        else:
            self.__debug("Got nothing!")
            raise RuntimeError()

    def send(self, dest_addr, event, payload, callback=None):
        dest_public_key = self.__get_destination_key(dest_addr)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(dest_addr)
        encryptor = PKCS1_OAEP.new(dest_public_key)
        encrypted_payload = encryptor.encrypt(payload)
        message = SocketMessage(self.addr, event, encrypted_payload)
        json = jsonpickle.encode(message)
        buffer = json.encode('UTF-8')
        client.send(buffer)
        if callback is None:
            return
        buffer = client.recv(1024)
        if len(buffer) > 0:
            json = buffer.decode('UTF-8')
            message = jsonpickle.decode(json)
            self.__debug('Got response from peer ' + str(dest_addr) + '.')
            callback(message)

from peer_socket import PeerSocket

if __name__ == "__main__":
    addr_1 = ('localhost', 5000)
    addr_2 = ('localhost', 6000)
    addr_3 = ('localhost', 6001)
    addr_4 = ('localhost', 6002)
    addr_5 = ('localhost', 6003)

    peer_1 = PeerSocket(addr_1)
    peer_2 = PeerSocket(addr_2)
    peer_3 = PeerSocket(addr_3)
    peer_4 = PeerSocket(addr_4)
    peer_5 = PeerSocket(addr_5)


    def greeting(sender_addr, message):
        print(str(sender_addr) + ' said ' + message)
        return 'hello!'

    def ping(sender_addr, message):
        print(str(sender_addr) + ' said ' + message)
        return b'hello!'


    def response(message):
        print('Got response ' + message)


    event = 'GREETING'
    peer_1.on(event, greeting)
    peer_2.on(event, greeting)
    peer_3.on(event, greeting)
    peer_4.on(event, greeting)
    peer_5.on(event, greeting)

    peer_1.send(addr_2, event, b'hi there!', response)
    peer_2.send(addr_1, event, b'hello from 2!', response)
    peer_3.send(addr_1, event, b'hello from 3!', response)
    peer_4.send(addr_1, event, b'hello from 4!', response)
    peer_5.send(addr_1, event, b'hello from 5!', response)
